# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    gcctest.sh                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ndombre <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/12 12:21:49 by ndombre           #+#    #+#              #
#    Updated: 2016/11/12 14:35:17 by ndombre          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

BASEDIR="../GNL_PRO/"

clang -Wall -Wextra -Werror -I $BASEDIR/libft/includes -I $BASEDIR/ -o ./get_next_line.o -c $BASEDIR/get_next_line.c
clang -Wall -Wextra -Werror -I $BASEDIR/libft/includes -I $BASEDIR/ -o ./main.o -c $1
clang -o a.out main.o get_next_line.o -I $BASEDIR/libft/includes -L $BASEDIR/libft/ -lft


