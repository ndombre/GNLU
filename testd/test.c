/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 16:57:52 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/10 17:15:25 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		main(int i, char **v)
{
	if (i != 1)
	{
		printf("error");
		return (0);
	}
	int fd = open("test", O_RDONLY);
	char *l = NULL;
	int i = 1;
	while (i != 0)
	{
		getnextligne(fd, &l);
		if (i == -1)
			write(2, "\n\n\n\n\n\n\nERROR\n\n\n\n\n\n\n", 19);
	}
	close(fd);
}
