/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 18:01:11 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/12 15:48:45 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"


#include "libft/libft.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int		main(int nv, char **vv)
{
	if (nv != 3)
	{
		printf("error argv\n");
		return (0);
	}
	int ret1 = 1;
	int ret2 = 1;
	char *t1 = NULL;
	char *t2 = NULL;
	int fd1 = open(vv[1], O_RDONLY);
	int fd2 = open(vv[2], O_RDONLY);
	while (ret1 && ret2)
	{
		ret1 = get_next_line(fd1, &t1);
		ret2 = get_next_line(fd2, &t2);
		if (ret1 == 0 || ret2 == 0)
			break ;
		if (strcmp(t1, t2) != 0)
			printf("%s--%s\n", t1, t2);
	}
	close(fd1);
	close(fd2);
}
