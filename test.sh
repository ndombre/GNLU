# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.sh                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ndombre <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/12 11:30:13 by ndombre           #+#    #+#              #
#    Updated: 2016/11/21 16:56:08 by ndombre          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

BASEDIR="../gnl/"

make -C $BASEDIR/libft/ fclean && make -C $BASEDIR/libft/

clang -Wall -Wextra -Werror -I $BASEDIR/libft/includes -I $BASEDIR/ -o ./get_next_line.o -c $BASEDIR/get_next_line.c
clang -Wall -Wextra -Werror -I $BASEDIR/libft/includes -I $BASEDIR/ -o ./main.o -c ./main.c
clang -o a.out main.o get_next_line.o -I $BASEDIR/libft/includes -L $BASEDIR/libft/ -lft
for file in $(ls testd/ | sort)
do
echo ====================== run testd/$file ==========================
./a.out testd/$file > outu
cat -e testd/$file > outr
diff outu outr
done

clang -Wall -Wextra -Werror -I $BASEDIR/libft/includes -I $BASEDIR/ -o ./main.o -c ./mainstdin.c
clang -o a.out main.o get_next_line.o -I $BASEDIR/libft/includes -L $BASEDIR/libft/ -lft
for file in $(ls testd/ | sort)
do
echo ====================== run testd/$file ==========================
cat testd/$file  | ./a.out > outu
cat -e testd/$file > outr
diff outu outr
done

rm outu
rm outr
rm a.out
rm main.o
rm get_next_line.o
