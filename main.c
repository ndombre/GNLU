/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ndombre <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 18:01:11 by ndombre           #+#    #+#             */
/*   Updated: 2016/11/21 16:56:52 by ndombre          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"


#include "libft/libft.h"
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int		main(int nv, char **vv)
{
	if (nv != 2)
	{
		printf("error argv\n");
		return (0);
	}
	int ret = 1;
	char *t = NULL;
	int fd = open(vv[1], O_RDONLY);
	while ((ret = get_next_line(fd, &t)))
	{
		if (t != NULL)
			ft_putstr(t);
		else
			ft_putstr("(null)");
		ft_putstr("$\n");
	}
	close(fd);
}
